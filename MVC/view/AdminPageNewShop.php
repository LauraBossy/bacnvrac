<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include '../view/include/Head.php';
    ?>
</head>

<body>

    <header>
        <?php include '../view/include/Header.php' ?>
    </header>

    <main>
        <div class="container-fluid">
            <?php include '../view/include/AlertMsg.php' ?>
            <section class="bs-docs-section card">
                <div class="card-header">
                    <div class=" col-xl-12 d-flex justify-content-between align-self-center m-0">
                        <h2 class="m-0">Création / modification d'un shop</h2>
                        <a class="align-self-center" href="manageshop"><button type="button" class="btn btn-secondary btn-sm">Liste des shop</button></a>
                    </div>
                </div>
                <div class="card-body">
                    <form class="" action="manageshop" method="POST">
                        <input name="shopId" type="hidden" value="<?= $shopToEdit['idshop'] ?? -1 ?>" />
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label">Nom du shop</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" value="<?= $shopToEdit['name'] ?? "" ?>" name="shopName" placeholder="Nom" required>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label">Site internet</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" value="<?= $shopToEdit['website'] ?? "" ?>" name="shopWebsite" placeholder="www.myshop.com" required>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-labell">Adresse email</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" value="<?= $shopToEdit['email'] ?? "" ?>" name="shopEmail" placeholder="contact@example.com" required>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label">Téléphone </label>
                            <div class="col-sm-8">
                                <input type="tel" class="form-control" value="<?= $shopToEdit['tel'] ?? "" ?>" name="shopTel" placeholder="04 95 43 55 26" required>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label">Adresse </label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-text">N°</span>
                                    <input type="number" class="form-control" value="<?= $shopToEdit['number'] ?? "" ?>" name="shopNumStreet" placeholder="12" required>
                                    <span class="input-group-text">Nom de la voie</span>
                                    <input type="text" class="form-control" value="<?= $shopToEdit['street'] ?? "" ?>" name="shopStreet" placeholder="impasse des Rosiers" required>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-text">Ville</span>
                                    <input type="text" class="form-control" value="<?= $shopToEdit['city'] ?? "" ?>" name="shopCity" placeholder="Marseille" required>
                                    <span class="input-group-text">Code postal</span>
                                    <input type="number" class="form-control" value="<?= $shopToEdit['citycode'] ?? "" ?>" name="shopCityCode" placeholder="13001" required>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-text">Département</span>
                                    <input type="text" class="form-control" value="<?= $shopToEdit['department'] ?? "" ?>" name="shopDepartement" placeholder="Bouches du Rhones" required>
                                    <span class="input-group-text">Région</span>
                                    <input type="text" class="form-control" value="<?= $shopToEdit['region'] ?? "" ?>" name="shopRegion" placeholder="PACA" required>
                                </div>
                            </div>
                        </div>
                        <div class="row p-2">
                            <button name="submit" value="saveshop" class="btn btn-primary" type="submit">Sauvegarder</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </main>

    <footer>
        <?php include '../view/include/Footer.php' ?>
    </footer>
</body>

</html>