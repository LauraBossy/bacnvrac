<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include '../view/include/Head.php';
    ?>
</head>

<body>

    <header>
        <?php include '../view/include/Header.php' ?>
    </header>

    <main>
        <div class="container-fluid">
            <?php include '../view/include/AlertMsg.php' ?>
            <section class="bs-docs-section card">
                <div class="card-header">
                    <div class=" col-xl-12 d-flex justify-content-between align-self-center m-0">
                        <h2 class="m-0">Liste des shop</h2>
                        <a class="align-self-center" href="manageshop?shop_id_to_edit=0"><button type="button" class="btn btn-secondary btn-sm">Nouveau shop</button></a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th scope=" col">Nom du magasin</th>
                                    <th scope="col">Site Web</th>
                                    <th class="text-center" scope="col">Modifier</th>
                                    <th class="text-center" scope="col">Supprimer</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($listShop as $shop) {
                                ?>
                                    <tr>
                                        <td><?= $shop['name'] ?></td>
                                        <td><?= $shop['website'] ?></td>
                                        <td class="text-center"><a href="manageshop?shop_id_to_edit=<?= $shop['idshop'] ?>"><img src="./img/pencil-square.svg" /></a></td>
                                        <td class="text-center"><a href="manageshop?shop_id_to_delete=<?= $shop['idshop'] ?>"><img src="./img/x-square.svg" /></a></td>
                                    </tr>

                                <?php
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
            </section>
        </div>
    </main>

    <footer>
        <?php include '../view/include/Footer.php' ?>
    </footer>
</body>

</html>