<?php
require_once('../models/ShopManager.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include '../view/include/Head.php' ?>
</head>

<body class="mvh-100">

    <header class="h-25">
        <?php include '../view/include/Header.php' ?>
    </header>

    <main class="h-50">
        <div class="container-fluid">
            <?php include '../view/include/AlertMsg.php' ?>
            <section class="bs-docs-section row d-flex">
                <div class="card border-light  order-sm-1 order-md-2 col-md-8 p-0 mb-3">
                    <div class="card-body p-2">
                        <iframe class="map__iframe" frameborder="0" style="border:0" referrerpolicy="no-referrer-when-downgrade" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyB16nGsW3mEVJ3vWP49OJ7byFajY0mTTD8&q=bio+shop+in+Marseile" allowfullscreen>
                        </iframe>
                    </div>
                </div>
                <div class=" col-md-4 order-sm-2 order-md-1 mb-3 ">
                    <form class="d-flex mb-3">
                        <input class="form-control me-2 " name="search" type="text" placeholder="Search" required>
                        <button class="btn btn-dark" type="submit">Search</button>
                    </form>
                    <div class="shop__scrollbar-ripe-malinka shop__list overflow-auto">
                        <?php
                        foreach ($shopList as $shop) {
                        ?>
                            <article class="card border-primary mb-3 me-2 ">
                                <h2 class="card-header"> <?php echo $shop["name"]; ?> </h2>
                                <div class="card-body">
                                    <p class="card-text"> Téléphone : <a href='tel:<?= $shop["tel"] ?>'> <?= $shop["tel"] ?> </a> </p>
                                    <p class="card-text"> Site web : <a href='<?= $shop["website"] ?>' target="_blank"> <?= $shop["website"] ?> </a> </p>
                                    <p class="card-text"> Adresse : <?= $shop["number"] . " " . $shop["street"] . " " . $shop["city"] . " " . $shop["citycode"] . " " . $shop["department"] ?> </p>
                                </div>
                            </article>
                        <?php
                        }
                        ?>
                    </div>
                </div>

            </section>
        </div>
    </main>

    <footer class="h-25 d-flex m-0">
        <?php include '../view/include/Footer.php' ?>
    </footer>
</body>