<section class="bs-docs-section row m-2">
    <?php if (isset($msg) && $msg != null) {
        $isError = is_array($msg);
        $alertType = $isError ? "alert-warning" : "alert-success";
        $msgToDisplay = $isError ? $msg['error'] : $msg;
    ?>
        <div class="alert alert-dismissible <?= $alertType ?>">
            <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
            <p class="mb-0"><?= $msgToDisplay ?></p>
        </div>
    <?php } ?>
</section>