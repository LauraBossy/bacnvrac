<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light row d-flex align-items-start">
        <div class="container-fluid col-8 col-lg-9 mt-2">
            <button class="navbar-toggler ms-3 mb-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse ms-3" id="navbarColor03">
                <ul class="navbar-nav me-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="home">Accueil
                            <span class="visually-hidden">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="findyourshop">Trouve ton shop</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Mon compte</a>
                        <div class="dropdown-menu">
                            <?php if (1 == $_SESSION['role']) { ?>
                                <a class="dropdown-item" href="login">S'identifier</a>
                            <?php } elseif (2 == $_SESSION['role']) { ?>
                                <a class="dropdown-item" href="myaccount">Mon compte</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="logout">Déconnexion</a>
                            <?php } elseif (3 == $_SESSION['role']) { ?>
                                <a class="dropdown-item" href="manageuser">Gérer les utilisateurs</a>
                                <a class="dropdown-item" href="manageshop">Gérer les magasins</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="logout">Déconnexion</a>
                            <?php } ?>
                        </div>

                    </li>
                </ul>

            </div>
        </div>
        <div class="col-4 col-lg-3">
            <h1 onclick="Music()" title="Ecoutez notre logo !" class="bacnvrac__h1">bacnvrac</h1>
        </div>
    </nav>
</div>