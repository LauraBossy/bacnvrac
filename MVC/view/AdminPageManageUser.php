<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include '../view/include/Head.php';
    ?>
</head>

<body>

    <header>
        <?php include '../view/include/Header.php' ?>
    </header>

    <main>
        <div class="container-fluid">
            <?php include '../view/include/AlertMsg.php' ?>
            <section class="bs-docs-section card">
                <div class="card-header">
                    <div class=" col-xl-12 d-flex justify-content-between align-self-center m-0">
                        <h2 class="m-0">Liste des utilisateurs</h2>
                        <a class="align-self-center" href="manageuser?user_id_to_edit=0"><button type="button" class="btn btn-secondary btn-sm">Nouvel utilisateur</button></a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th scope=" col">Pseudo</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Role</th>
                                    <th class="text-center" scope="col">Modifier</th>
                                    <th class="text-center" scope="col">Supprimer</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($listUsers as $user) {
                                ?>
                                    <tr>
                                        <td><?= $user['pseudo'] ?></td>
                                        <td><?= $user['email'] ?></td>
                                        <?php if (2 == $user['user_role_id']) { ?>
                                            <td>Utilisateur</td>
                                        <?php } elseif (3 == $user['user_role_id']) { ?>
                                            <td>Administrateur</td>
                                        <?php } else { ?>
                                            <td>Invité</td>
                                        <?php } ?>
                                        <td class="text-center"><a href="manageuser?user_id_to_edit=<?= $user['iduser'] ?>"><img src="./img/pencil-square.svg" /></a></td>
                                        <td class="text-center"><a href="manageuser?user_id_to_delete=<?= $user['iduser'] ?>"><img src="./img/x-square.svg" /></a></td>
                                    </tr>

                                <?php
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
            </section>
        </div>
    </main>

    <footer>
        <?php include '../view/include/Footer.php' ?>
    </footer>
</body>

</html>