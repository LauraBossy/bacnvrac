<!DOCTYPE html>
<html lang="en">

<head>
    <?php include '../view/include/Head.php' ?>
</head>

<body class="vh-100">

    <header class="h-25">
        <?php include '../view/include/Header.php' ?>
    </header>

    <main class="h-50">
        <div class="container-fluid ">
            <?php include '../view/include/AlertMsg.php' ?>
            <section class="bs-docs-section avatars__section">
                <img class="avatar__img" src="./img/Laura.png" alt="Laura">
                <img class="avatar__img" src="./img/Raphael.png" alt="Raphael">
                <img class="avatar__img" src="./img/Mathilde.png" alt="Mathilde">
            </section>
            <section class="bs-docs-section mt-4">
                <?php if (isset($_SESSION['iduser'])) {
                ?>
                    <p class="text-success text-center">
                        Bonjour <?= $_SESSION['pseudo'] ?>,
                    </p>
                    <p class="text-secondary text-center">
                        Laura, Raphael et Mathilde te souhaite une belle expérience !
                    </p>
                <?php } else {
                ?>
                    <p class="text-success text-center">
                        Bienvenue sur le site de Laura, Raphael et Mathilde réalisé dans la cadre du projet CUBES 3
                    </p>
                <?php }
                ?>

            </section>
        </div>
    </main>

    <footer class="h-25">
        <?php include '../view/include/Footer.php' ?>
    </footer>
</body>

</html>