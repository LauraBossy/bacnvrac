<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include '../view/include/Head.php';
    ?>
</head>

<body>

    <header>
        <?php include '../view/include/Header.php' ?>
    </header>

    <main>
        <div class="container-fluid">
            <?php include '../view/include/AlertMsg.php' ?>
            <section class="bs-docs-section card">
                <div class="card-header">
                    <div class=" col-xl-12 d-flex justify-content-between align-self-center m-0">
                        <h2 class="m-0">Création / modification d'un utilisateur</h2>
                        <a class="align-self-center" href="manageuser"><button type="button" class="btn btn-secondary btn-sm">Liste des utilisateurs</button></a>
                    </div>
                </div>
                <div class="card-body">
                    <form class="" action="manageuser" method="POST">
                        <input name="iduser" type="hidden" value="<?= $userToEdit['iduser'] ?? -1 ?>" />
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label">Pseudo</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" value="<?= $userToEdit['pseudo'] ?? "" ?>" name="pseudo" placeholder="Pseudo" required>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label">Adresse email</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" value="<?= $userToEdit['email'] ?? "" ?>" name="email" placeholder="contact@example.com" required>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">Rôle</label>
                            <div class="col-sm-8">
                                <select class="form-select" name="user_role_id">
                                    <option value="1" <?= isset($userToEdit['user_role_id']) && 1 == $userToEdit['user_role_id'] ? "selected" : "" ?>>Invité</option>
                                    <option value="2" <?= isset($userToEdit['user_role_id']) && 2 == $userToEdit['user_role_id'] ? "selected" : "" ?>>Utilisateur</option>
                                    <option value="3" <?= isset($userToEdit['user_role_id']) && 3 == $userToEdit['user_role_id'] ? "selected" : "" ?>>Administrateur</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label">Mot de passe</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="userPassword" id="userPassword" required>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="" class="col-sm-2 col-form-label">Confirmez le mot de passe</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="checkUserPassword" required>
                            </div>
                        </div>
                        <div class="row p-2">
                            <button name="submit" value="saveuser" class="btn btn-primary" type="submit">Sauvegarder</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </main>

    <footer>
        <?php include '../view/include/Footer.php' ?>
    </footer>
</body>

</html>