<!DOCTYPE html>
<html lang="en">

<head>
    <?php include '../view/include/Head.php' ?>
</head>

<body>

    <header>
        <?php include '../view/include/Header.php' ?>
    </header>

    <main>
        <div class="container-fluid">
            <?php include '../view/include/AlertMsg.php' ?>
            <section class="bs-docs-section row">
                <div class="col-lg-6">
                    <form class=" card border-primary mb-3" id="connexion" action="login" method="post">

                        <fieldset class="card-body">

                            <legend>Connectez-vous !</legend>

                            <div class="form-group">
                                <label for="exampleInputEmail1" class="form-label mt-4">Identifiant :</label>
                                <input type="email" class="form-control" id="connexionEmail" aria-describedby="emailHelp" name="emailconnexion" placeholder="Entrez votre email" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1" class="form-label mt-4">Mot de passe :</label>
                                <input type="password" class="form-control" id="connexionPassword" name="mdpconnexion" placeholder="Mot de passe" required>
                            </div>

                            <div class="btn-group-vertical mt-4">
                                <button type="submit" name="submit" value="connexionToAnAccount" class="btn btn-primary">Connexion</button>
                            </div>

                        </fieldset>
                    </form>
                </div>
                <div class="col-lg-6">
                    <form class=" card border-primary mb-3" id="accountCreation" action="login" method="post">
                        <fieldset class="card-body">
                            <legend>Pas encore de compte ? Créez le !</legend>

                            <div class="form-group">
                                <label for="exampleInputEmail1" class="form-label mt-4">Addresse Email :</label>
                                <input type="email" class="form-control" aria-describedby="emailHelp" name="email" value="<?= $_POST['email'] ?? '' ?>" placeholder="Entrez votre email" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1" class="form-label mt-4">Pseudo :</label>
                                <input type="text" class="form-control" name="pseudo" value="<?= $_POST['pseudo'] ?? '' ?>" placeholder="Pseudo" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1" class="form-label mt-4">Mot de passe :</label>
                                <input type="password" class="form-control" name="mdp" value="<?= $_POST['mdp'] ?? '' ?>" placeholder="Mot de passe" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1" class="form-label mt-4">Répétez votre mot de passe :</label>
                                <input type="password" class="form-control" name="checkmdp" value="<?= $_POST['checkmdp'] ?? '' ?>" placeholder="Mot de passe" required>
                            </div>

                            <div class="btn-group-vertical mt-4">
                                <button type="submit" name="submit" value="createUser" class="btn btn-primary">S'enregister</button>

                            </div>

                        </fieldset>
                    </form>
                </div>

            </section>

        </div>
    </main>

    <footer>
        <?php include '../view/include/Footer.php' ?>
    </footer>
</body>

</html>