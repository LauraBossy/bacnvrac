<!DOCTYPE html>
<html lang="en">

<head>
    <?php include '../view/include/Head.php' ?>
</head>

<body class="vh-100">

    <header class="h-25">
        <?php include '../view/include/Header.php' ?>
    </header>

    <main class="h-50">
        <div class="container-fluid">
            <?php include '../view/include/AlertMsg.php' ?>
            <section class="bs-docs-section">
            </section>
            <section class="bs-docs-section ">
            </section>
        </div>
    </main>

    <footer class="h-25 d-flex">
        <?php include '../view/include/Footer.php' ?>
    </footer>
</body>

</html>