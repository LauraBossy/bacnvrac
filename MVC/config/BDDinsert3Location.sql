INSERT INTO location (idlocation, street, city, citycode, department) 
    VALUES (1, 'Etienne Rabattu', 'Plan de Campagne', 13170, 'Bouches-du-Rhone');
INSERT INTO location (idlocation, number, street, city, citycode, department) 
    VALUES (2, 4, 'Pierre de Coubertin', 'Aix-en-Provence', 13100, 'Bouches-du-Rhone');
INSERT INTO location (idlocation, number, street, city, citycode, department) 
    VALUES (3, 135, 'Boulevard Bara', 'Marseille', 13013, 'Bouches-du-Rhone');
INSERT INTO location (idlocation, number, street, city, citycode, department) 
    VALUES (4, 7, 'Avenue President Kennedy', 'La Ciotat', 13600, 'Bouches-du-Rhone');
INSERT INTO location (idlocation, street, city, citycode, department) 
    VALUES (5, 'Avenue Albin Gilles', 'Saint-Remy-de-Provence', 13210, 'Bouches-du-Rhone');
INSERT INTO location (idlocation, street, city, citycode, department) 
    VALUES (6, 'Chemin departemental 559', 'Saint-Cyr-sur-Mer', 83270,  'Var');
INSERT INTO location (idlocation, number, street, city, citycode, department) 
    VALUES (7, 2, 'Rempart', 'Hyeres', 83400, 'Var');
INSERT INTO location (idlocation, street, city, citycode, department) 
    VALUES (8, "Avenue Corniche d'Azur", 'Saint-Aygulf', 83370, 'Var');
INSERT INTO location (idlocation, number, street, city, citycode, department) 
    VALUES (9, 26, 'Pierre Senard', 'Toulon', 83000, 'Var');