
INSERT INTO user_role (id_user_role, role_name) VALUES 
(1, 'Guest'),
(2, 'User'),
(3, 'Admin');


INSERT INTO user (iduser, pseudo, email, password, user_role_id) VALUES
    (1, 'Raphael', 'raphaelcavatore@gmail.com', 'f71dbe52628a3f83a77ab494817525c6', 3),
    (2, 'Laura', 'laurabossy.dev@gmail.com', 'f71dbe52628a3f83a77ab494817525c6', 3),
    (3, 'Mathilde', 'sasia.mathilde@gmail.com', 'f71dbe52628a3f83a77ab494817525c6', 3), 
    (4, 'UserTest', 'userTest@gmail.com', 'f71dbe52628a3f83a77ab494817525c6', 2);


INSERT INTO user_allow_action (id_user_allow_action, action_name) VALUES
    (1, 'home'),
    (2, 'findyourshop'),
    (3, 'login'),
    (4, 'logout'),
    (5, 'api'),
    (6, 'myaccount'),
    (7, 'manageshop'),
    (8, 'manageuser');

INSERT INTO user_allow_action_has_user_role (user_allow_action_id, user_role_id) VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (2, 1),
    (2, 2),
    (2, 3),
    (3, 1),
    (3, 2),
    (3, 3),
    (4, 2),
    (4, 3),
    (5, 1),
    (5, 2),
    (5, 3),
    (6, 2),
    (7, 3),
    (8, 3);