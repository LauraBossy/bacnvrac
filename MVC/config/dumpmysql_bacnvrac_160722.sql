-- MariaDB dump 10.19  Distrib 10.4.22-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: bacnvrac
-- ------------------------------------------------------
-- Server version	10.4.22-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie` (
  `idcategorie` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(45) NOT NULL,
  PRIMARY KEY (`idcategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,'vrac'),(2,'bio'),(3,'local');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `idlocation` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `street` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `citycode` int(11) NOT NULL,
  `department` varchar(45) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idlocation`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,0,'Etienne Rabattu','Plan de Campagne',13170,'Bouches-du-Rh├┤ne',NULL),(2,4,'Pierre de Coubertin','Aix-en-Provence',13100,'Bouches-du-Rh├┤ne',NULL),(3,135,'Boulevard Bara','Marseille',13013,'Bouches-du-Rh├┤ne',NULL),(4,7,'Avenue Pr├®sident Kennedy','La Ciotat',13600,'Bouches-du-Rh├┤ne',NULL),(5,0,'Avenue Albin Gilles','Saint-R├®my-de-Provence',13210,'Bouches-du-Rh├┤ne',NULL),(6,0,'Chemin d├®partemental 559','Saint-Cyr-sur-Mer',83270,'Var',NULL),(7,2,'Rempart','Hy├¿res',83400,'Var',NULL),(8,0,'Avenue Corniche d\'Azur','Saint-Aygulf',83370,'Var',NULL),(9,26,'Pierre S├®nard','Toulon',83000,'Var',NULL);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop` (
  `idshop` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `location_idlocation` int(11) DEFAULT NULL,
  PRIMARY KEY (`idshop`),
  KEY `fk_shop_location1` (`location_idlocation`),
  CONSTRAINT `fk_shop_location1` FOREIGN KEY (`location_idlocation`) REFERENCES `location` (`idlocation`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop`
--

LOCK TABLES `shop` WRITE;
/*!40000 ALTER TABLE `shop` DISABLE KEYS */;
INSERT INTO `shop` VALUES (1,'Marcel&Fils Bio','http://www.marceletfils.com',NULL,'0442772740',1),(2,'Ma Terre','http://www.materre.net/',NULL,'0442268365',2),(3,'BioCoop Sitebio','http://www.biocoop.fr',NULL,'0491058265',3),(4,'La Vie Claire','http://www.lavieclaire.com',NULL,'0442709877',4),(5,'Alpilles Bio','http://www.biomonde.fr',NULL,'0432600776',5),(6,'Chez Erick','https://epicurien.com/pages/nous-trouver',NULL,'0483361592',6),(7,'La Famille tout en Vrac','https://www.facebook.com/familletoutenvrac',NULL,'0494385036',7),(8,'Croq Nature','http://www.magasin-bio-saint-aygulf.fr',NULL,'0494817843',8),(9,'Les Thes du Monde','https://diptikana-les-thes-du-monde.business.',NULL,'0489796246',9);
/*!40000 ALTER TABLE `shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_has_categorie`
--

DROP TABLE IF EXISTS `shop_has_categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_has_categorie` (
  `shop_idshop` int(11) NOT NULL,
  `categorie_idcategorie` int(11) NOT NULL,
  PRIMARY KEY (`shop_idshop`,`categorie_idcategorie`),
  KEY `fk_shop_has_categorie_categorie1` (`categorie_idcategorie`),
  CONSTRAINT `fk_shop_has_categorie_categorie1` FOREIGN KEY (`categorie_idcategorie`) REFERENCES `categorie` (`idcategorie`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shop_has_categorie_shop1` FOREIGN KEY (`shop_idshop`) REFERENCES `shop` (`idshop`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_has_categorie`
--

LOCK TABLES `shop_has_categorie` WRITE;
/*!40000 ALTER TABLE `shop_has_categorie` DISABLE KEYS */;
INSERT INTO `shop_has_categorie` VALUES (1,2),(2,2),(2,3),(3,1),(3,2),(3,3),(4,2),(5,2),(6,2),(7,1),(8,2),(9,1);
/*!40000 ALTER TABLE `shop_has_categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (33,'toto','sasia.mathilde@gmail.com','f71dbe52628a3f83a77ab494817525c6','2'),(34,'toto','toto@toto.com','f71dbe52628a3f83a77ab494817525c6','1');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_has_shop`
--

DROP TABLE IF EXISTS `user_has_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_has_shop` (
  `user_iduser` int(11) NOT NULL,
  `shop_idshop` int(11) NOT NULL,
  PRIMARY KEY (`user_iduser`,`shop_idshop`),
  KEY `fk_user_has_shop2_shop1` (`shop_idshop`),
  CONSTRAINT `fk_user_has_shop2_shop1` FOREIGN KEY (`shop_idshop`) REFERENCES `shop` (`idshop`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_shop2_user1` FOREIGN KEY (`user_iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_shop`
--

LOCK TABLES `user_has_shop` WRITE;
/*!40000 ALTER TABLE `user_has_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_has_shop` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-16 11:41:43
