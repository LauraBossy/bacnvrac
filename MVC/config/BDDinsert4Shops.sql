INSERT INTO shop (idshop, name, website, tel, location_idlocation) 
    VALUES (1, 'Marcel&Fils Bio', 'http://www.marceletfils.com', '0442772740', 1);
INSERT INTO shop (idshop, name, website, tel, location_idlocation) 
    VALUES (2, 'Ma Terre', 'http://www.materre.net/', '0442268365', 2);
INSERT INTO shop (idshop, name, website, tel, location_idlocation) 
    VALUES (3, 'BioCoop Sitebio', 'http://www.biocoop.fr', '0491058265', 3);
INSERT INTO shop (idshop, name, website, tel, location_idlocation) 
    VALUES (4, 'La Vie Claire', 'http://www.lavieclaire.com', '0442709877', 4);
INSERT INTO shop (idshop, name, website, tel, location_idlocation) 
    VALUES (5, 'Alpilles Bio', 'http://www.biomonde.fr', '0432600776', 5);
INSERT INTO shop (idshop, name, website, tel, location_idlocation) 
    VALUES (6, 'Chez Erick', 'https://epicurien.com/pages/nous-trouver', '0483361592', 6);
INSERT INTO shop (idshop, name, website, tel, location_idlocation) 
    VALUES (7, 'La Famille tout en Vrac', 'https://www.facebook.com/familletoutenvrac', '0494385036', 7);
INSERT INTO shop (idshop, name, website, tel, location_idlocation) 
    VALUES (8, 'Croq Nature', 'http://www.magasin-bio-saint-aygulf.fr', '0494817843', 8);
INSERT INTO shop (idshop, name, website, tel, location_idlocation) 
    VALUES (9, 'Les Thes du Monde', 'https://diptikana-les-thes-du-monde.business.site', '0489796246', 9);