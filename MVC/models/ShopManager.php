<?php
require_once("../models/PDO.php");

function GetAllShop($search = null)
{
    global $PDO;
    $req = "SELECT * FROM shop INNER JOIN location ON shop.location_idlocation = location.idlocation";
    if (isset($search) && !empty($search)) {
        $req .= " where concat(COALESCE(name,''), COALESCE(website,''), COALESCE(email,''), COALESCE(tel,''),"
            . " COALESCE(street,''), COALESCE(city,''), COALESCE(citycode,''), COALESCE(department,''),"
            . " COALESCE(region,'')) like :search";
        $preparedReq = $PDO->prepare($req);
        $preparedReq->execute(
            array(
                "search" => "%" . $search . "%"
            )
        );
    } else {
        $preparedReq = $PDO->prepare($req);
        $preparedReq->execute();
    }

    $allShop = $preparedReq->fetchAll(PDO::FETCH_ASSOC);
    return $allShop;
}

function GetShopByName($name)
{
    global $PDO;
    $req = "SELECT * from shop INNER JOIN location ON shop.idshop = location.idlocation WHERE name = :name";
    $preparedReq = $PDO->prepare($req);
    $preparedReq->execute(
        array(
            'name' => $name
        )
    );
    $oneShop = $preparedReq->fetchAll(PDO::FETCH_ASSOC);
    return $oneShop;
}

function GetShopById($id)
{
    global $PDO;
    $req = "SELECT * from shop INNER JOIN location ON shop.location_idlocation = location.idlocation WHERE idshop = :id";
    $preparedReq = $PDO->prepare($req);
    $preparedReq->execute(
        array(
            'id' => $id
        )
    );
    $oneShop = $preparedReq->fetch(PDO::FETCH_ASSOC);
    return $oneShop;
}

function CreateShop($name, $website, $email, $tel, $numberstreet, $street, $city, $citycode, $department, $region)
{
    global $PDO;

    $reqLocation = "INSERT INTO location (number, street, city, citycode, department, region) "
        . "VALUES (:numberstreet, :street, :city, :citycode, :department, :region)";
    $preparedReqLocation = $PDO->prepare($reqLocation);
    $responseLocation = $preparedReqLocation->execute(
        array(
            "numberstreet" => $numberstreet,
            "street" => $street,
            "city" => $city,
            "citycode" => $citycode,
            "department" => $department,
            "region" => $region
        )
    );
    if ($responseLocation === false) {
        $msg = [
            "error" => "ko:" . implode($preparedReqLocation->errorInfo()),
            "code" => 500
        ];
        return $msg;
    }
    $locationId = $PDO->lastInsertId();

    $reqShop = "INSERT INTO shop (name, website, email, tel, location_idlocation) "
        . "VALUES (:name, :website, :email, :tel, :locationId)";
    $preparedReqShop = $PDO->prepare($reqShop);
    $preparedReqShop->execute(
        array(
            "name" => $name,
            "website" => $website,
            "email" => $email,
            "tel" => $tel,
            "locationId" => $locationId
        )
    );
    if ($responseLocation === false) {
        $msg = [
            "error" => "ko:" . implode($preparedReqLocation->errorInfo()),
            "code" => 500
        ];
        return $msg;
    }
    $msg = "Création du shop réussie";
    return $msg;
}

function DeleteShop($idshop)
{
    global $PDO;

    $shopToDelete = GetShopById($idshop);
    $idlocation = $shopToDelete['idlocation'];

    $reqCategorie = "DELETE from shop_has_categorie WHERE shop_idshop = :idshop";
    $preparedReqCategorie = $PDO->prepare($reqCategorie);
    $responseReqCategorie = $preparedReqCategorie->execute(
        array(
            "idshop" => $idshop,
        )
    );
    if ($responseReqCategorie == false) {
        $msg = [
            "error" => "ko:" . implode($preparedReqCategorie->errorInfo()),
            "code" => 500
        ];
        return $msg;
    }

    $reqShop = "DELETE from shop WHERE idshop = :idshop";
    $preparedReqShop = $PDO->prepare($reqShop);
    $responseReqShop = $preparedReqShop->execute(
        array(
            "idshop" => $idshop,
        )
    );
    if ($responseReqShop == false) {
        $msg = [
            "error" => "ko:" . implode($preparedReqShop->errorInfo()),
            "code" => 500
        ];
        return $msg;
    }

    $reqLocation = "DELETE FROM location WHERE idlocation = :idlocation";
    $preparedReqLocation = $PDO->prepare($reqLocation);
    $responseLocation = $preparedReqLocation->execute(
        array(
            "idlocation" => $idlocation,
        )
    );
    if ($responseLocation == false) {
        $msg = [
            "error" => "ko:" . implode($preparedReqLocation->errorInfo()),
            "code" => 500
        ];
        return $msg;
    }

    $msg = "Suppression du shop réussie";
    return $msg;
}

function UpdateShop($idShop, $name, $website, $email, $tel, $numberstreet, $street, $city, $citycode, $department, $region)
{
    global $PDO;

    $shopToUpdate = GetShopById($idShop);
    $idLocation = $shopToUpdate['location_idlocation'];

    $reqLocation = "UPDATE location set number = :numberstreet, street = :street, city = :city, citycode = :citycode, department = :department, region = :region WHERE idlocation = :idlocation";
    $preparedReqLocation = $PDO->prepare($reqLocation);
    $responseLocation = $preparedReqLocation->execute(
        array(
            "numberstreet" => $numberstreet,
            "street" => $street,
            "city" => $city,
            "citycode" => $citycode,
            "department" => $department,
            "region" => $region,
            "idlocation" => $idLocation
        )
    );
    if ($responseLocation === false) {
        $msg = [
            "error" => "ko:" . implode($preparedReqLocation->errorInfo()),
            "code" => 500
        ];
        return $msg;
    }
    $reqShop = "UPDATE shop set name = :name, website = :website, email = :email, tel = :tel WHERE idshop = :idshop AND location_idlocation = :idlocation";
    $preparedReqShop = $PDO->prepare($reqShop);
    $preparedReqShop->execute(
        array(
            "name" => $name,
            "website" => $website,
            "email" => $email,
            "tel" => $tel,
            "idshop" => $idShop,
            "idlocation" => $idLocation
        )
    );
    if ($responseLocation === false) {
        $msg = [
            "error" => "ko:" . implode($preparedReqLocation->errorInfo()),
            "code" => 500
        ];
        return $msg;
    }
    $msg = "Modification du shop réussie";
    return $msg;
}
