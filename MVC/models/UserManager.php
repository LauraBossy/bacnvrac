<?php
require_once 'PDO.php';

function GetAllUsers(&$msg = NULL)
{
    global $PDO;
    $sql = "SELECT * FROM user";
    $preparedReq = $PDO->prepare($sql);
    $success = $preparedReq->execute();
    $allUsers = $preparedReq->fetchAll(PDO::FETCH_ASSOC);
    return $allUsers;
}

function GetallowActionForOneRole($userRole)
{
    global $PDO;
    $sql = "SELECT user_allow_action.action_name FROM user_allow_action_has_user_role  INNER JOIN user_allow_action "
        . "ON id_user_allow_action = user_allow_action_id "
        . "WHERE user_role_id = :userRole";
    $preparedReq = $PDO->prepare($sql);
    $success = $preparedReq->execute(
        array(
            "userRole" => $userRole,
        )
    );
    if ($success === false) {
        return array();
    }
    $allowActionForOneRole = $preparedReq->fetchAll(PDO::FETCH_ASSOC);
    $allowActionForOneRole = array_column($allowActionForOneRole, 'action_name');
    return $allowActionForOneRole;
}

function CheckUserPermission($userRole, $action, $redirection = true)
{
    if (!isset($userRole)) {
        $userRole = 1; // Guest
    }
    $allowActionForOneRole = GetallowActionForOneRole($userRole);
    $resultArraySearch = array_search($action, $allowActionForOneRole, true);
    if (false === $resultArraySearch) {
        if ($redirection) {
            header("location: login");
            die();
        } else {
            return false;
        }
    } else {
        return true;
    }
}

function GetUserByEmail($email, &$msg = NULL)
{
    global $PDO;
    $sql = "SELECT * FROM user WHERE email = :email";
    $preparedReq = $PDO->prepare($sql);
    $success = $preparedReq->execute(
        array(
            "email" => $email,
        )
    );
    if ($success === false) {
        $msg = [
            "error" => "ko:" . implode($preparedReq->errorInfo()),
            "code" => 500
        ];
        return;
    }
    $userForOneEmail = $preparedReq->fetch(PDO::FETCH_ASSOC);
    return $userForOneEmail;
}

function GetUserById($iduser, &$msg = NULL)
{
    global $PDO;
    $sql = "SELECT * FROM user WHERE iduser = :iduser";
    $preparedReq = $PDO->prepare($sql);
    $success = $preparedReq->execute(
        array(
            "iduser" => $iduser,
        )
    );
    if ($success === false) {
        $msg = [
            "error" => "ko:" . implode($preparedReq->errorInfo()),
            "code" => 500
        ];
        return;
    }
    $userForOneid = $preparedReq->fetch(PDO::FETCH_ASSOC);
    return $userForOneid;
}

function CreateUser($pseudo, $mdp, $email, &$msg = NULL)
{
    global $PDO;
    $userForOneEmail = GetUserByEmail($email);

    if (isset($userForOneEmail['email']) && $userForOneEmail['email'] == $email) {
        $msg = [
            "error" => "Email déjà utilisé",
            "code" => 200
        ];
        $newUsers = [];
    } else {
        $sql = "INSERT INTO user (pseudo, email, password, user_role_id) "
            . "VALUES (:pseudo, :email, :password, '2')";
        $preparedReq = $PDO->prepare($sql);
        $success = $preparedReq->execute(
            array(
                "pseudo" => $pseudo,
                "email" => $email,
                "password" => md5($mdp)
            )
        );
        if ($success === false) {
            $msg = [
                "error" => "ko:" . implode($preparedReq->errorInfo()),
                "code" => 500
            ];
            return;
        }
        $newUserId = $PDO->lastInsertId();
        $newUser = GetUserById($newUserId);
        $_SESSION['iduser'] = $newUser['iduser'];
        $_SESSION['pseudo'] = $newUser['pseudo'];
        $_SESSION['role'] = $newUser['user_role_id'];
    }
    return ($newUsers);
}

function ConnexionToAnAccount($emailconnexion, $mdpconnexion, &$msg = NULL)
{
    global $PDO;
    $sql = "SELECT * FROM user WHERE email = :email AND password = :mdp;";
    $preparedReq = $PDO->prepare($sql);
    $response = $preparedReq->execute(
        array(
            "email" => $emailconnexion,
            'mdp' => md5($mdpconnexion)
        )
    );
    if ($response == false) {
        $msg = [
            "error" => "ko:" . implode($preparedReq->errorInfo()),
            "code" => 500
        ];
    } else {
        $connectedUser = $preparedReq->fetch(PDO::FETCH_ASSOC);
        if ($connectedUser == null) {
            $msg = [
                "error" => "Mot de passe ou email incorrect",
                "code" => 200
            ];
        } else {
            $_SESSION['iduser'] = $connectedUser['iduser'];
            $_SESSION['pseudo'] = $connectedUser['pseudo'];
            $_SESSION['role'] = $connectedUser['user_role_id'];
        }
    }
    return $connectedUser;
}

function DeleteUser($iduser)
{
    global $PDO;

    $reqUser = "DELETE from user WHERE iduser = :iduser";
    $preparedReqUser = $PDO->prepare($reqUser);
    $reponseReqUser = $preparedReqUser->execute(
        array(
            "iduser" => $iduser,
        )
    );
    if ($reponseReqUser == false) {
        $msg = [
            "error" => "ko:" . implode($preparedReqUser->errorInfo()),
            "code" => 500
        ];
        return $msg;
    }

    $msg = "Suppression de l'utilisateur réussie";
    return $msg;
}

function UpdateUser($idUser, $pseudo, $email, $password, $userRoleId)
{
    global $PDO;

    $reqUser = "UPDATE user set pseudo = :pseudo, email = :email, "
        . "password = :password, user_role_id = :userRoleId WHERE iduser = :iduser";
    $preparedReqUser = $PDO->prepare($reqUser);
    $responseReqUser = $preparedReqUser->execute(
        array(
            "pseudo" => $pseudo,
            "email" => $email,
            "password" => md5($password),
            "userRoleId" => $userRoleId,
            "iduser" => $idUser
        )
    );
    if ($responseReqUser === false) {
        $msg = [
            "error" => "ko:" . implode($preparedReqUser->errorInfo()),
            "code" => 500
        ];
        return $msg;
    }
    $msg = "Modification de l'utilisateur réussie";
    return $msg;
}

function CreateUserByAdmin($pseudo, $email, $userRole, $password)
{
    global $PDO;
    $userForOneEmail = GetUserByEmail($email);
    $msg = "Modification de l'utilisateur réussie";
    if (isset($userForOneEmail['email']) && $userForOneEmail['email'] == $email) {
        $msg = "Email déjà utilisé";
    } else {
        $sql = "INSERT INTO user (pseudo, email, password, user_role_id) "
            . "VALUES (:pseudo, :email, :password, :userRole)";
        $preparedReq = $PDO->prepare($sql);
        $success = $preparedReq->execute(
            array(
                "pseudo" => $pseudo,
                "email" => $email,
                "password" => md5($password),
                "userRole" => $userRole
            )
        );
        if ($success === false) {
            $msg = [
                "error" => "ko:" . implode($preparedReq->errorInfo()),
                "code" => 500
            ];
        }
    }
    return ($msg);
}
