<?php
require_once '../models/UserManager.php';

$loggedUser = [
    "iduser" => -1,
    "user_role_id" => 1
];
$userIdFromToken = GetUserIdFromToken();
if ($userIdFromToken > 0) {
    $loggedUser = GetUserById($userIdFromToken);
}
$allowedAction = CheckUserPermission($loggedUser['user_role_id'], $actionApi, false);

if (!$allowedAction) {
    $data = ["error" => "Vous n'etes pas autorisé à faire cette action."];
} else {
    switch ($actionApi) {

        case 'findyourshop':
            $data = GetAllShop($_GET['search'] ?? null);
            break;

        case 'login':
            $emailconnexion = $_GET['emailconnexion'] ?? '';
            $mdpconnexion = $_GET['mdpconnexion'] ?? '';
            $User = ConnexionToAnAccount($emailconnexion, $mdpconnexion, $msg);
            $thereIsAnError = $msg != NULL && is_array($msg);
            if ($thereIsAnError) {
                $data = $msg;
                break;
            }
            $token = GenerateToken($User['iduser']);
            $data = ["jwt" => $token];
            break;

        case 'manageuser':
            $data = GetAllUsers();
            break;

        default:
            $data = ["error" => "Your request is not valide, check url"];
            break;
    }
}

include_once '../view/api/apiJson.php';
