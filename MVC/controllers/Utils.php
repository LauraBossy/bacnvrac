<?php

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

function ErrorMsg($msg, $httpCode = 200)
{
    $msg = [
        "error" => $msg,
        "code" => $httpCode
    ];
    return $msg;
}

function sendJson($infos)
{
    header("Access-Control-Allow-Origin: * ");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    echo json_encode($infos, JSON_UNESCAPED_UNICODE);
}

$JWT_KEY = 'backnvrac_secret_key';
$JWT_ALGO = 'HS256';

function GenerateToken($userId)
{
    global $JWT_KEY;
    global $JWT_ALGO;
    $data = [
        "iss" => "bacnvrac", // Issuer => celui qui emet le token
        "iat" => time(), // Issued At => la datetime de création du token
        "nbf" => time() + 1, // Not Before => à partir de quand il est valide
        "exp" => time() + 60 * 30, // Expiration Time => quand il ne sera plus valide, ici 30 minutes
        // partie custom ajoutée pour stocker le user et l'id
        "user" => array(
            "id" => $userId
        )
    ];
    return JWT::encode($data, $JWT_KEY, $JWT_ALGO);
}

function GetUserIdFromToken()
{
    global $JWT_KEY;
    global $JWT_ALGO;
    $headers = apache_request_headers();
    if (!isset($headers['authorization'])) {
        return 0;
    }
    $authHeader = $headers['authorization'];
    $arr = explode(" ", $authHeader);
    if (!isset($arr[1])) {
        return -1;
    }
    try {
        $token = $arr[1];
        $data = JWT::decode($token, new Key($JWT_KEY, $JWT_ALGO));
        if (isset($data) && isset($data->user) && isset($data->user->id)) {
            return $data->user->id;
        }
    } catch (Exception $e) {
    }
    return -1;
}
