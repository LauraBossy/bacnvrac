<?php

require_once '../vendor/autoload.php';
require_once '../models/UserManager.php';
require_once '../models/ShopManager.php';
require_once 'Utils.php';

$action = substr(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), 1);
$actionApi = substr($action, 4);
if (!isset($_SESSION['role'])) {
    $_SESSION['role'] = 1;
}
$_SESSION['goBackAction'] = $action;

switch ($action) {

    default:
    case 'home':
        CheckUserPermission($_SESSION['role'], $action);
        include "../view/Home.php";
        break;

    case 'login':
        CheckUserPermission($_SESSION['role'], $action);
        if (isset($_POST['submit'])) {
            switch ($_POST['submit']) {
                case 'connexionToAnAccount':
                    $emailconnexion = $_POST['emailconnexion'];
                    $mdpconnexion = $_POST['mdpconnexion'];
                    $User = ConnexionToAnAccount($emailconnexion, $mdpconnexion, $msg);
                    if ($msg == NULL) {
                        header("location: home");
                    }
                    break;
                case 'createUser':
                    $email = $_POST['email'];
                    $pseudo = $_POST['pseudo'];
                    $mdp =  $_POST['mdp'];
                    $checkmdp = $_POST['checkmdp'];
                    if ($mdp != $checkmdp) {
                        $msg = ErrorMsg("Les mots de passe ne sont pas identiques");
                    } else {
                        $User = CreateUser($pseudo, $mdp, $email, $msg);
                        if ($msg == NULL) {
                            header("location: home");
                        }
                    }
                    break;
            }
        }
        include("../view/LoginPage.php");
        break;

    case 'logout':
        CheckUserPermission($_SESSION['role'], $action);
        $_SESSION = [];
        session_destroy();
        header("location: login");
        break;

    case 'findyourshop':
        CheckUserPermission($_SESSION['role'], $action);
        $shopList = GetAllShop($_GET['search'] ?? null);
        include("../view/FindYourShop.php");
        break;

    case 'myaccount':
        CheckUserPermission($_SESSION['role'], $action);
        $userToEdit = GetUserById($_SESSION['iduser']);
        if (isset($_POST['submit'])) {
            if (empty($_POST['pseudo'])) {
                $msg = ErrorMsg("Le champ Pseudo est obligatoire.");
                include "../view/AccountUserPage.php";
                break;
            }
            if (empty($_POST['email'])) {
                $msg = ErrorMsg("Le champ Adresse email est obligatoire.");
                include "../view/AccountUserPage.php";
                break;
            }
            if ($_POST['userPassword'] != $_POST['checkUserPassword'] || empty($_POST['userPassword'])) {
                $msg = ErrorMsg("Les mots de passe ne sont pas identiques ou vides");
                include "../view/AccountUserPage.php";
                break;
            }
            $msg = UpdateUser(
                $_POST['iduser'],
                $_POST['pseudo'],
                $_POST['email'],
                $_POST['userPassword'],
                $_POST['user_role_id']
            );
        }
        include "../view/AccountUserPage.php";
        break;

    case 'manageshop':
        CheckUserPermission($_SESSION['role'], $action);
        if (isset($_GET['shop_id_to_edit']) && $_GET['shop_id_to_edit'] == 0) {
            include "../view/AdminPageNewShop.php";
        } elseif (isset($_GET['shop_id_to_edit']) && $_GET['shop_id_to_edit'] != 0) {
            $shopToEdit = GetShopById($_GET['shop_id_to_edit']);
            include "../view/AdminPageNewShop.php";
        } elseif (isset($_POST['submit'])) {
            if ($_POST['shopId'] < 0) {
                $msg = CreateShop(
                    $_POST['shopName'],
                    $_POST['shopWebsite'],
                    $_POST['shopEmail'],
                    $_POST['shopTel'],
                    $_POST['shopNumStreet'],
                    $_POST['shopStreet'],
                    $_POST['shopCity'],
                    $_POST['shopCityCode'],
                    $_POST['shopDepartement'],
                    $_POST['shopRegion'],
                );
            } elseif ($_POST['shopId'] > 0) {
                $msg = UpdateShop(
                    $_POST['shopId'],
                    $_POST['shopName'],
                    $_POST['shopWebsite'],
                    $_POST['shopEmail'],
                    $_POST['shopTel'],
                    $_POST['shopNumStreet'],
                    $_POST['shopStreet'],
                    $_POST['shopCity'],
                    $_POST['shopCityCode'],
                    $_POST['shopDepartement'],
                    $_POST['shopRegion'],
                );
            }
            $listShop = GetAllShop();
            include "../view/AdminPageManageShop.php";
        } else {
            if (isset($_GET['shop_id_to_delete'])) {
                $msg = DeleteShop($_GET['shop_id_to_delete']);
            }
            $listShop = GetAllShop();
            include "../view/AdminPageManageShop.php";
        }
        break;

    case 'manageuser':
        CheckUserPermission($_SESSION['role'], $action);

        if (isset($_GET['user_id_to_edit'])) {
            if ($_GET['user_id_to_edit'] > 0) {
                $userToEdit = GetUserById($_GET['user_id_to_edit']);
            }
            include "../view/AdminPageNewUser.php";
            break;
        }

        if (isset($_POST['submit'])) {
            $askToCreateNewUser = $_POST['iduser'] <= 0 || !isset($_POST['iduser']);
            if (!$askToCreateNewUser) {
                $userToEdit = GetUserById($_POST['iduser']);
            }
            if (empty($_POST['pseudo'])) {
                $msg = ErrorMsg("Le champ Pseudo est obligatoire.");
                include "../view/AdminPageNewUser.php";
                break;
            }
            if (empty($_POST['email'])) {
                $msg = ErrorMsg("Le champ Adresse email est obligatoire.");
                include "../view/AdminPageNewUser.php";
                break;
            }
            if ($_POST['userPassword'] != $_POST['checkUserPassword'] || empty($_POST['userPassword'])) {
                $msg = ErrorMsg("Les mots de passe ne sont pas identiques ou vides");
                include "../view/AdminPageNewUser.php";
                break;
            }
            if ($askToCreateNewUser) {
                $msg = CreateUserByAdmin(
                    $_POST['pseudo'],
                    $_POST['email'],
                    $_POST['user_role_id'],
                    $_POST['userPassword']
                );
            } else {
                $msg = UpdateUser(
                    $_POST['iduser'],
                    $_POST['pseudo'],
                    $_POST['email'],
                    $_POST['userPassword'],
                    $_POST['user_role_id']
                );
            }
            $thereIsAnError = is_array($msg);
            if ($thereIsAnError) {
                include "../view/AdminPageNewUser.php";
            } else {
                $listUsers = GetAllUsers();
                include "../view/AdminPageManageUser.php";
            }
        } else {
            if (isset($_GET['user_id_to_delete'])) {
                $msg = DeleteUser($_GET['user_id_to_delete']);
            }
            $listUsers = GetAllUsers();
            include "../view/AdminPageManageUser.php";
        }
        break;

    case 'api/' . $actionApi:
        require "../controllers/ApiController.php";
        break;
}
