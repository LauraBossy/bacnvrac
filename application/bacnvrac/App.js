import Home from './views/Home';
import FindYourShop from './views/FindYourShop';
import Login from './views/Login';
import Register from './views/Register';
import ManageShops from './views/ManageShops';
import UsersList from './views/UsersList';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useState } from 'react';
import { jwt, context } from './service/context';
import { AppNavigator } from './routes/drawer';

export default function App() {

  const [token, setToken] = useState()
  jwt.token = token
  jwt.setToken = setToken

  return (
    <AppNavigator />
  );

}