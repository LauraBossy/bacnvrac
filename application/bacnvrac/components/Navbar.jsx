import { StyleSheet, View, Text } from 'react-native'
import React from 'react'
import Hamburger from './Hamburger';

const Navbar = () => {

    return (
        <View style={styles.navbar}>
            <Hamburger />
            <Text style={styles.logo}> bacnvrac </Text>
        </View>
    )
}

export default Navbar

const styles = StyleSheet.create({
    navbar: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: "100%",
        height: 70,
        backgroundColor: '#78c2ad'
    },

    // Style for the logo bacnvrac
    logo: {
        color: "#DCDCDC",
        fontSize: 32
    }
})
