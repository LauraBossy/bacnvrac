import { View, Pressable, StyleSheet } from 'react-native'
import { useState } from 'react'
import React from 'react'

// TODO : set SearchBar in burger button
// import SearchBar from '../components/SearchBar';

const Hamburger = () => {

    const [hamburgerOpen, setHamburgerOpen] = useState(false);

    const ToggleHamburger = ({ navigation }) => {
        setHamburgerOpen(!hamburgerOpen)
    }
    return (
        <View >
            <Pressable style={styles.panneau} onPressOut={ToggleHamburger}>
                <View style={styles.hamburger} />
                <View style={styles.hamburger} />
                <View style={styles.hamburger} />

                {/* Il faudrait que ce qui suit soit dans l'hamburger */}
                {/* <SearchBar /> */}

            </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    hamburger: {
        width: 30,
        height: 5,
        backgroundColor: '#78c2ad',
        margin: 2,
        borderRadius: 10,
        alignItems: 'center'
    },
    panneau: {
        backgroundColor: "#DCDCDC",
        flexBasis: 40,
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 1,
        borderRadius: 5
    }
});

export default Hamburger