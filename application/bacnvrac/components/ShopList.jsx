import { View, Text, FlatList, StyleSheet } from 'react-native'
import React, { useEffect, useState } from 'react'

const ShopList = (props) => {
    return (
        <View style={styles.view}>
            <FlatList
                data={props.shops}
                renderItem={({ item, index, separators }) => {
                    return (
                        <View style={styles.shop}>
                            <Text style={styles.shopname}>{item.name}</Text>
                            <Text>{item.website} </Text>
                            <Text> {item.tel}</Text>
                            <Text>{item.number} {item.street} {item.city} {item.citycode} {item.department} </Text>
                        </View>
                    )
                }
                }
            ></FlatList>
        </View>
    )
}

const styles = StyleSheet.create({
    shop: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 150,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        padding: 20,
        width: '80%',
        alignSelf: 'center',
        alignItems: 'center'
    },
    view: {
        width: '90%',
        height: '40%',
        borderColor: 'black',
        borderTopWidth: 1,
        borderBottomWidth: 1
    },
    shopname: {
        fontWeight: 'bold',
        fontSize: 20
    }
});

export default ShopList