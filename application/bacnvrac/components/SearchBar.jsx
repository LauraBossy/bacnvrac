import { View, Image, TextInput, StyleSheet, Text } from 'react-native'
import { useState } from 'react'
import React from 'react'

const SearchBar = (props) => {
    const [text, onChangeText] = React.useState('')
    const [search, setSearch] = useState()
    const [shopView, setShopView] = useState()

    const updateShopList = (searchText) => {
        console.log('Update shop list for:' + searchText)
        props.searchFunction(searchText)
    }

    return (
        <View style={styles.searchBarBndIcon}>
            <Image
                style={styles.searchIcon}
                source={{
                    uri: 'https://icons.iconarchive.com/icons/icons8/windows-8/512/Logos-Google-Web-Search-icon.png',
                }} />
            <TextInput
                style={styles.searchbar}
                value={search}
                placeholder="Chercher un magasin"
                keyboardType='default'
                onSubmitEditing={() => updateShopList(search)}
                onChangeText={setSearch}
            />
        </View>
    )
}

const styles = StyleSheet.create({

    // Style for the research
    searchbar: {
        height: 30,
        width: "70%",
        borderWidth: 1
    },
    searchBarBndIcon: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchIcon: {
        width: 32,
        height: 32
    }
})

export default SearchBar