import { View, Image, StyleSheet } from 'react-native'
import React from 'react'

const MobileMaps = () => {
    return (
        <View>
            <Image style={styles.maps}
                source={require('../assets/maps.png')}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    maps: {
        width: 300,
        height: 200
    }
});

export default MobileMaps