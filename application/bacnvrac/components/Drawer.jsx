import 'react-native-gesture-handler';
import { createDrawerNavigator } from '@react-navigation/drawer';
import 'react-native-reanimated';

const Drawer = createDrawerNavigator();

function MyDrawer() {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Feed" component={Feed} />
            <Drawer.Screen name="Article" component={Article} />
        </Drawer.Navigator>
    );
}

export default MyDrawer;