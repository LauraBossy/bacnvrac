import { View, Text, FlatList, StyleSheet, Button } from 'react-native'
import React, { useEffect, useState } from 'react'

const ManageList = () => {

    const [shops, setShops] = useState({});
    // const [search, setSearch] = useState('');

    // const onInputChange = e => {
    //     setSearch(e.target.value);
    // }

    useEffect(() => {
        fetch('http://bacnvrac.com/api/findyourshop')
            .then((response) => response.json())
            .then((data) => {
                setShops(data)
            })
    }, [])

    return (
        <View style={styles.view}>
            <FlatList
                data={shops}
                renderItem={({ item, index, separators }) => {
                    return (
                        <View style={styles.shop}>
                            <Text style={styles.shopname}>{item.name}</Text>
                            <Text>{item.website} </Text>
                            <Text> {item.tel}</Text>
                            <Text>{item.number} {item.street} {item.city} {item.citycode} {item.department} </Text>
                            <View style={styles.buttonContainer}>
                                <Button
                                    // onPress={onPressLearnMore}
                                    title="Modifier"
                                    color="#78c2ad"
                                />
                                <Button
                                    // onPress={onPressLearnMore}
                                    title="Supprimer"
                                    color="#78c2ad"
                                />
                            </View>
                        </View>
                    )
                }
                }
            ></FlatList>
        </View>
    )
}

const styles = StyleSheet.create({
    shop: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 200,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        padding: 20,
        width: '80%',
        alignSelf: 'center',
        alignItems: 'center'
    },
    view: {
        width: '90%',
        height: '80%',
        borderColor: 'black',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        marginBottom: 30
    },
    shopname: {
        fontWeight: 'bold',
        fontSize: 20
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-around',
    }
});

export default ManageList