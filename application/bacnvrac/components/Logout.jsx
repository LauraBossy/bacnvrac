import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native'
import { useState, useContext } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { jwt, context } from '../service/context'

const Logout = () => {

    const contextLogout = useContext(context)

    const logout = () => {
        contextLogout.setToken(null)
        AsyncStorage.clear();
        console.log('Logout :' + jwt.token);
    };

    return (
        <View>
            <TouchableOpacity
                style={styles.button}
                onPress={logout}
            >
                <Text style={styles.button__text}> Se déconnecter </Text>
            </TouchableOpacity>
        </View>
    )
}

export default Logout

const styles = StyleSheet.create({
    button: {
        margin: 15,
        padding: 10,
        height: 40,
        width: 200,
        borderRadius: 15,
        Color: '#78c2ad',
        backgroundColor: '#66a593'
    },

    button__text: {
        fontWeight: "bold",
        textAlign: 'center',
        color: 'white',
    }
})