import { View, Image, StyleSheet } from 'react-native'
import React from 'react'

const Avatars = () => {
    return (
        <View style={styles.containerAvatars}>
            <Image style={styles.avatar}
                source={require('../assets/Laura.png')}
            />
            <Image style={styles.avatar}
                source={require('../assets/Raphael.png')}
            />
            <Image style={styles.avatar}
                source={require('../assets/Mathilde.png')}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    avatar: {
        width: 50,
        height: 50
    },
    containerAvatars: {
        alignSelf: 'center',
        flexDirection: 'row',
        width: 300,
        justifyContent: 'space-around'
    }
})

export default Avatars