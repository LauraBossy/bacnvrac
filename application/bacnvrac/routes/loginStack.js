import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginPage from '../views/Login';

const { Navigator, Screen } = createStackNavigator();

export const LoginStack = () => (
    <Navigator
        headerMode='screen'
        screenOptions={{
            headerStyle: {
                backgroundColor: '#eee',
            },
            headerTintColor: '#444',
            height: 60
        }}
    >
        <Screen
            name='Login'
            component={LoginPage}
            options={{ title: 'Se connecter' }}
        />
    </Navigator>
);

export default LoginStack;