import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomePage from '../views/Home';
import Register from '../views/Register';

const { Navigator, Screen } = createStackNavigator();

export const CreerStack = () => (
    <Navigator
        headerMode='screen'
        screenOptions={{
            headerStyle: {
                backgroundColor: '#eee',
            },
            headerTintColor: '#444',
            height: 60
        }}
    >
        <Screen
            name='creeraccount'
            component={Register}
            options={{ title: 'Créer un compte' }}
        />
    </Navigator>
);

export default CreerStack;