import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomePage from '../views/Home';
import LoginPage from '../views/Login';

const { Navigator, Screen } = createStackNavigator();

export const HomeStack = () => (
    <Navigator
        headerMode='screen'
        screenOptions={{
            headerStyle: {
                backgroundColor: '#eee',
            },
            headerTintColor: '#444',
            height: 60
        }}
    >
        <Screen
            name='Home'
            component={HomePage}
            options={{ title: 'Bacnvrac' }}
        />
        <Screen
            name='Login'
            component={LoginPage}
            options={{ title: 'Login page' }}
        />
    </Navigator>
);

export default HomeStack;