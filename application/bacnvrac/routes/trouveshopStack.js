import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import FindYourShop from '../views/FindYourShop';

const { Navigator, Screen } = createStackNavigator();

export const FindyourshopStack = () => (
    <Navigator
        headerMode='screen'
        screenOptions={{
            headerStyle: {
                backgroundColor: '#eee',
            },
            headerTintColor: '#444',
            height: 60
        }}
    >
        <Screen
            name='findyourshop'
            component={FindYourShop}
            options={{ title: 'Trouver ton shop' }}
        />
    </Navigator>
);

export default FindyourshopStack;