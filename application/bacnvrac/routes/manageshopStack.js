import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomePage from '../views/Home';
import Manageshop from '../views/ManageShops';

const { Navigator, Screen } = createStackNavigator();

export const manageshopStack = () => (
    <Navigator
        headerMode='screen'
        screenOptions={{
            headerStyle: {
                backgroundColor: '#eee',
            },
            headerTintColor: '#444',
            height: 60
        }}
    >
        <Screen
            name='manageshop'
            component={Manageshop}
            options={{ title: 'Gérer mes shops' }}
        />
    </Navigator>
);

export default LoginStack;