import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import HomeStack from './homeStack';
import LoginStack from './loginStack';
import CreerStack from './creerStack';
import FindyourshopStack from './trouveshopStack';
import UserslistStack from './userslistStack';
import ManageshopStack from '../views/ManageShops';
import UsersList from '../views/UsersList';
import { View } from 'react-native-web';
import { jwt, context } from '../service/context';
const { Navigator, Screen } = createDrawerNavigator();

export const RootDrawerNavigator = () => (
    <Navigator initialRouteName='Home'>
        <Screen
            name='Accueil'
            component={HomeStack}
        />
        <Screen
            name='Trouve ton shop'
            component={FindyourshopStack}
        />
        <Screen
            name='Gérer mes shops'
            component={ManageshopStack}
        />
        <Screen
            name='Liste des utilisateurs'
            component={UserslistStack}
        />
        <Screen
            name='Se connecter'
            component={LoginStack}
        />
        <Screen
            name='Créer un compte'
            component={CreerStack}
        />
    </Navigator>
);

export const AppNavigator = () => (

    <NavigationContainer>
        <context.Provider value={jwt}>
            <RootDrawerNavigator />
        </context.Provider>
    </NavigationContainer>
);

export default AppNavigator;