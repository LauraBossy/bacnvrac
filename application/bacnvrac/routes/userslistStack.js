import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomePage from '../views/Home';
import UsersList from '../views/UsersList';

const { Navigator, Screen } = createStackNavigator();

export const UserslistStack = () => (
    <Navigator
        headerMode='screen'
        screenOptions={{
            headerStyle: {
                backgroundColor: '#eee',
            },
            headerTintColor: '#444',
            height: 60
        }}
    >
        <Screen
            name='userslist'
            component={UsersList}
            options={{ title: 'Liste des utilisateurs' }}
        />
    </Navigator>
);

export default UserslistStack;