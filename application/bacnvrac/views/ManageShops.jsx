import { View, StyleSheet } from 'react-native'
import React from 'react'
import ManageList from '../components/ManageList';
import Logout from '../components/Logout';

const FindYourShop = () => {
    return (
        <View style={styles.findyourshop}>
            <Logout />
            <ManageList />
        </View>
    )
}

const styles = StyleSheet.create({

    findyourshop: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})

export default FindYourShop