import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native'
import { useState } from 'react'

const Register = () => {

    const [loginId, setLoginId] = useState('user@ex.com')
    const [pseudo, setPseudo] = useState('Toto')
    // const [password, setPassword] = useState('user')

    return (
        <View>
            <View style={styles.main}>
                <Text> Pas encore de compte ? Créez-le ! </Text>
                <TextInput
                    style={styles.input}
                    value={loginId}
                    onChangeText={setLoginId}
                    keyboardType='email-address'
                />
                <TextInput
                    style={styles.input}
                    value={pseudo}
                    onChangeText={setPseudo}
                    keyboardType='default'
                />
                <TextInput
                    style={styles.input}
                    // value={password}
                    // onChangeText={setPassword}
                    placeholder='Votre mot de passe'
                    keyboardType='default'
                    secureTextEntry={true}
                />
                <TextInput
                    style={styles.input}
                    // value={password}
                    // onChangeText={setPassword}
                    keyboardType='default'
                    placeholder='Répétez votre mot de passe'
                    secureTextEntry={true}
                />

                <TouchableOpacity style={styles.button}>
                    <Text style={styles.button__text}> Se connecter </Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    main: {
        height: '90%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        margin: 15,
        padding: 10,
        height: 40,
        width: 200,
        borderRadius: 15,
        borderWidth: 2,
        borderColor: '#66a593'
    },

    button: {
        margin: 15,
        padding: 10,
        height: 40,
        width: 200,
        borderRadius: 15,
        Color: '#78c2ad',
        backgroundColor: '#66a593'
    },

    button__text: {
        fontWeight: "bold",
        textAlign: 'center',
        color: 'white',
    }

})