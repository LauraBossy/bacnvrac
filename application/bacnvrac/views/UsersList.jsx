import { View, Text, StyleSheet, FlatList, Button } from 'react-native'
import React, { useState, useEffect, useRef } from 'react'
import Navbar from '../components/Navbar';
import { storeData, getData, USER_KEY, IP_API_SERVER } from '../service/storageService'
import { jwt, context } from '../service/context';

const UsersList = () => {

    const [users, setUsers] = useState({});

    const callApiManageUser = () => {
        let urlToCall = 'http://' + IP_API_SERVER + '/api/manageuser'

        fetch(urlToCall, {
            headers: {
                'Authorization': `Bearer ${jwt.token}`,
            },
        })
            .then((response) => response.json())
            .then((data) => {
                setUsers(data)
            });
    };

    useEffect(() => {
        callApiManageUser()
    }, [])

    return (
        <View style={styles.view}>
            <FlatList
                data={users}
                renderItem={({ item, index, separators }) => {
                    return (
                        <View style={styles.user}>
                            <Text style={styles.username}>{item.pseudo}</Text>
                            <Text>{item.email} </Text>
                            <Text> {item.user_role_id}</Text>
                            <View style={styles.buttonContainer}>
                                <Button
                                    // onPress={onPressLearnMore}
                                    title="Modifier"
                                    color="#78c2ad"
                                />
                                <Button
                                    // onPress={onPressLearnMore}
                                    title="Supprimer"
                                    color="#78c2ad"
                                />
                            </View>
                        </View>
                    )
                }
                }
            ></FlatList>
        </View>
    )
}

const styles = StyleSheet.create({
    user: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 200,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        padding: 20,
        width: '80%',
        alignSelf: 'center',
        alignItems: 'center'
    },
    view: {
        width: '90%',
        height: '80%',
        borderColor: 'black',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        marginBottom: 30
    },
    username: {
        fontWeight: 'bold',
        fontSize: 20
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-around',
    }
});

export default UsersList