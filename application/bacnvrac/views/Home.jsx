import { View, Text, Button, StyleSheet } from 'react-native'
import React from 'react';
import Avatars from '../components/Avatars';
import { storeData, getData, USER_KEY, IP_API_SERVER } from '../service/storageService'

const HomePage = ({ navigation }) => {
    return (
        <View style={styles.HomePage}>
            <View style={styles.halfPage}>
                <View>
                    <Avatars />
                    <Text style={styles.welcome}> Bienvenue sur l'appli de Laura, Raphael et Mathilde réalisé dans la cadre du projet CUBES 3  </Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    HomePage: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    halfPage: {
        height: '49%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    halfPageToDelete: {
        height: '49%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    welcome: {
        color: '#78c2ad',
        marginTop: 20,
        textAlign: 'center'
    }
})

export default HomePage