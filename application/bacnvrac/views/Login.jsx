import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native'
import { useState, useEffect, useContext } from 'react'
import Navbar from '../components/Navbar'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { storeData, getData, USER_KEY, IP_API_SERVER } from '../service/storageService'
import { jwt, context } from '../service/context'
import Logout from '../components/Logout'

const LoginPage = ({ navigation }) => {

    const [loginEmail, setLoginEmail] = useState('sasia.mathilde@gmail.com')
    const [password, setPassword] = useState('toto')
    const contextLogin = useContext(context)

    const login = () => {
        let urlToCall = 'http://' + IP_API_SERVER + '/api/login?emailconnexion=' + loginEmail + '&mdpconnexion=' + password
        //console.log('email:' + loginEmail + '/ mdp : ' + password + '/ url :' + urlToCall)
        fetch(urlToCall)
            .then((response) => response.json())
            .then((data) => {
                if (data != '') {
                    const currentToken = data.jwt
                    contextLogin.setToken(currentToken)
                    storeData(USER_KEY, data).then()
                    navigation.navigate('Accueil')
                    console.log('login :' + jwt.token)

                } else {
                    console.log("error")
                }
            });

    };

    return (
        <View>
            {jwt.token ? (
                <Logout />
            ) : (
                <View style={styles.main}>
                    <Text> Connectez-vous ! </Text>
                    <TextInput
                        style={styles.input}
                        value={loginEmail}
                        onChangeText={setLoginEmail}
                        keyboardType='email-address'
                    />
                    <TextInput
                        style={styles.input}
                        value={password}
                        onChangeText={setPassword}
                        keyboardType='default'
                        secureTextEntry={true}
                    />
                    <TouchableOpacity
                        style={styles.button}
                        onPress={login}
                    >
                        <Text style={styles.button__text}> Se connecter </Text>
                    </TouchableOpacity>
                </View>
            )}
        </View >
    )
}

export default LoginPage

const styles = StyleSheet.create({
    main: {
        height: '90%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        margin: 15,
        padding: 10,
        height: 40,
        width: 200,
        borderRadius: 15,
        borderWidth: 2,
        borderColor: '#66a593'
    },

    button: {
        margin: 15,
        padding: 10,
        height: 40,
        width: 200,
        borderRadius: 15,
        Color: '#78c2ad',
        backgroundColor: '#66a593'
    },

    button__text: {
        fontWeight: "bold",
        textAlign: 'center',
        color: 'white',
    }

})