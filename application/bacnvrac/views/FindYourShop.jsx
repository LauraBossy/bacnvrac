import { View, Text, StyleSheet } from 'react-native'
import React, { useState, useEffect, useRef } from 'react'
import MobileMaps from '../components/MobileMaps';
import ShopList from '../components/ShopList';
import SearchBar from '../components/SearchBar';
import Navbar from '../components/Navbar';
import { storeData, getData, USER_KEY, IP_API_SERVER } from '../service/storageService'

const FindYourShop = () => {
    const [shops, setShops] = useState({});

    const callApiSearch = (strToSearch) => {
        let urlToCall = 'http://' + IP_API_SERVER + '/api/findyourshop'
        if (strToSearch != '') {
            urlToCall += '?search=' + strToSearch
        }
        fetch(urlToCall)
            .then((response) => response.json(urlToCall))
            .then((data) => {
                setShops(data)
            })
    };

    useEffect(() => {
        callApiSearch('')
    }, [])

    return (
        <View
            style={styles.findyourshop}>
            <SearchBar searchFunction={callApiSearch} />
            <ShopList shops={shops} />
            <MobileMaps />
        </View>
    )
}

const styles = StyleSheet.create({

    findyourshop: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})

export default FindYourShop